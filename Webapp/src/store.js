import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const state = {
    istAdmin : 1,
    sessionId: 1,
    nutzername: "",
    personalnummer: 0,
    nummer_optionen: []
};

const mutations = {
    logout (state) {
        state.sessionId = null;
        state.istAdmin = 0;
        state.nutzername = "";
        state.personalnummer = 0;
        state.nummer_optionen = [];
    },
    login (state, payload) {
        state.sessionId = payload.cid;
        state.nutzername = payload.name;
        state.istAdmin = payload.admin;
        state.personalnummer = payload.personalnummer;
    },
    optionen_festlegen (state, payload) {
        state.nummer_optionen = payload.optionen;
    }
};

const actions = {
    logout: ({commit}) => commit('logout'),
    login: ({commit}) => commit('login'),
    optionen_festlegen: ({commit}) => commit('optionen_festlegen')
};


const getters = {

};

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations,
})