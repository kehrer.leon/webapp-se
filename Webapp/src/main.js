import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import axios from "axios"
import VueAxios from "vue-axios"
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import store from "./store";
import Login from './components/Login.vue'
import MitarbeiterBearbeiten from './components/MitarbeiterBearbeiten.vue'
import MitarbeiterEntfernen from './components/MitarbeiterEntfernen.vue'
import MitarbeiterHinzufuegen from './components/MitarbeiterHinzufuegen.vue'
import NutzerBearbeiten from './components/NutzerBearbeiten.vue'

Vue.http = axios;
Vue.use(BootstrapVue, VueAxios, axios);

new Vue({
    el: '#app',
    store,
    components:{Login, MitarbeiterBearbeiten, MitarbeiterEntfernen, MitarbeiterHinzufuegen, NutzerBearbeiten},
    render: h => h(App)
});
